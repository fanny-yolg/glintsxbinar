// ## 1. Convert Minute to time
// set second
// minute/60 - minute % 60 = min
// minute - (min * 60) = sec

function convertMinute(minute) {
  const min = Math.floor(minute / 60);
  const sec = minute - min * 60;
  return min + ':' + sec;
}

//Test Case
console.log(convertMinute(100)); //1:40
console.log(convertMinute(185)); //3:05

// ## 2. Sort the letters
const sortLetter = (str) => {
  let huruf = 'abcdefghijklmnopqrstuvwxyz';
  let kata = '';
  for (let i = 0; i < huruf.length; i++) {
    for (let j = 0; j < str.length; j++) {
      if (huruf[i] === str[j]) {
        kata += str[j];
      }
    }
  }
  return kata;
};

// Test Case
console.log(sortLetter('hello')); //ehllo

// ## 3. A random color
//Global
let colors = ['red', 'green', 'blue'];
const randomColor = (colors) => {
  let randomNum = Math.floor(Math.random() * colors.length);
  return colors[randomNum];
};

//Test Case
console.log(randomColor(colors)); //Output -> Color : red

// ## 4. Split words without function .split(" ") and find the max
var string = 'Lorem ipsum is dummy text';

// Using For

// Using While
function maxWordLength() {
  var maxWord = '';
  var nextWord = '';
}

// Test Case
maxWordLength(string); //Lorem

// ## 5. Count the vowel
function vowelsObject(apaja) {
  let value = {
    a: 0,
    i: 0,
    u: 0,
    e: 0,
    o: 0,
  };
  for (let i = 0; i < apaja.length; i++)
    switch (apaja[i]) {
      case 'a':
        value.a++;
        break;
      case 'i':
        value.i++;
        break;
      case 'u':
        value.u++;
        break;
      case 'e':
        value.e++;
        break;
      case 'o':
        value.o++;
        break;
    }
  console.log(value);
}
// function countVoewl(apaja) {}

//Test Case
vowelsObject('rum raisin chocolate ice cream');
/*
            {
                a : 3,
                i : 3,
                u : 1,
                e : 3,
                o : 2
        */

// ## 4. Split words without function .split(" ") and find the max
var string = 'Lorem ipsummmm is dummy text';

// Using While
// Using While
function maxWordLength(words) {
  let wordsArray = [];
  let newWord = '';
  let maxWord = '';

  for (let i = 0; i < words.length; i++) {
    if (words[i] !== ' ') {
      newWord += words[i];
    } else {
      wordsArray.push(newWord);
      newWord = '';
    }
  }

  for (let i = 0; i < wordsArray.length - 1; i++) {
    if (wordsArray[i].length >= wordsArray[i + 1].length) {
      if (maxWord.length !== wordsArray[i].length) {
        maxWord = wordsArray[i];
      }
    }
  }

  console.log(maxWord);
}

// Test Case
maxWordLength(string); //Lorem
