// ## 1. Check odd or even number

var num;
function typeOfNum(num) {
  if (num % 2 === 0) {
    return 'Even';
  } else {
    return 'Odd';
  }
}
console.log(typeOfNum(10));

// ## 2. Print the n first numbers

var num = 15;
var x = 0;
while (x < num) {
  x++;
  if (x % 3 === 0 && x % 5 === 0) {
    console.log(x + ' kelipatan 3 dan 5');
  } else if (x % 3 === 0) {
    console.log(x + ' kelipatan 3');
  } else if (x % 5 === 0) {
    console.log(x + ' kelipatan 5');
  } else {
    console.log(x);
  }
}

// ## 3. Print Segitiga

var angka = 10;
for (let i = 0; i < angka; i++) {
  var temp = '*';
  for (let j = 0; j < i; j++) {
    temp = temp + '*';
  }
  console.log(temp);
}

// ## 4. split words without function .split(" ")
var string = 'Lorem ipsum is dummy text';
var temp = [];
var words = '';
for (let i = 0; i < string.length; i++) {
  if (string[i] != ' ') {
    words += string[i];
  } else {
    temp.push(words);
    words = '';
  }
  if (string[i] == string.length - 1) {
    temp.push(words);
  }
}
console.log(temp);

// ## 5. Find the faktorial
var nilai = 5;
var temp = '';
var total = 1;
for (let i = nilai; i > 0; i--) {
  if (i != 1) {
    temp += `${i}.`;
    total = total * i;
  } else {
    temp += i;
    total = total * 1;
  }
}

console.log(temp, total);
// Faktorial
// 3! = 3.2.1 = 6

//Input 5
//Output
// 5.4.3.2.1 = 120
